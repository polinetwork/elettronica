# Elettronica
`Italian Version`

Ciao, questa è la repository dedicata alla Laurea Triennale in Ingegneria Elettronica (Polimi) gestita dal PoliNetwork.
Ci troverete materiale riguardante i vari corsi e potrete caricarlo anche voi attraverso il bot telegram [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

- Primo anno https://gitlab.com/polinetwork/elettronica1y
 
- Secondo anno https://gitlab.com/polinetwork/elettronica2y
 
- Terzo anno https://gitlab.com/polinetwork/elettronica3y


`English Version`

Hello everyone this is the repository of BSc in Electronic Engineering (Polimi) reserved by PoliNetwork.
You will be able to find and upload materials for your courses (Notes, Exercises, ...).
To upload files use the Telegram Bot [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

- First year https://gitlab.com/polinetwork/elettronica1y
 
- Second year https://gitlab.com/polinetwork/elettronica2y
 
- Third year https://gitlab.com/polinetwork/elettronica3y


`DISCLAIMER`
**You mustn't upload any copyright protected or in any other author protection form cover file on this repository or any of the ones associated to it.**

Polinetwork takes no responsibility for and we do not expressly or implicitly endorse, support, or guarantee the completeness, truthfulness, accuracy, or reliability of any of the content on this repository or any of the ones associated to it.
